% - TP1: Trajectory generation and implementation using differential flatness

%% Trajectory generation

% A class of functions commonly used is represented by polynomials in the time
% variable – ti, which are capable of representing curves in a flexible way. 
% However, they have a poor numerical performance, their dimension depends on 
% the number of conditions imposed on the inputs, states and their derivatives.

% A different class of polynomials for representing a curve is given by Bésier basis functions. 
% The equivalence in terms of degrees of freedom is enhanced from the numerical condition point of view.
% The limitation of Bésier curves is related again to the need of higher degrees 
% to satisfy a large number of inequality and/or equality constraints

% It is more convenient to represent the flat output z(t) using B-splines, 
% because of their ease of enforcing continuity across way-points and 
% ease of computing their derivatives. 
% Moreover, their degree depends only up to which derivative is needed to 
% ensure continuity, this being in contrast with the polynomial 
% basis functions previously mentioned.

%% Matlab implementation

% 2D case for 3-Dof model of an UAV:
% xdot      =   Va * cos(psi)
% ydot      =   Va * sin(psi)
% psidot    =   g * tan(phi) / Va


% State: Xi = [x y psi]
% Input: u = [Va phi]

% Flat output: z = [z1 z2] = [x y] = [Xi_1 Xi_2] = f_0(Xi)
% => Xi = [z1 z2 atan(z2dot/z1dot)] = f_1(z, zdot)
% => u = [sqrt(z1dot^2 + z2dot^2) atan( (z2ddot*z1dot - z1ddot*z2dot) /
% sqrt(z1dot^2 + z2dot^2) )] = f_2(z, zdot, zddot)

% => differential flat system



% initialize the system parameters
params.g = 9.81;        % gravitational acceleration
params.h = 0.05;        % sampling time
params.dbl_Alt = 150;   % double altitude

limits.x = [-200,200];          % x-axis [m]
limits.y = [-200,200];          % y-axis [m]
limits.psi = [0,2*pi];          % (head)yaw angle [rad]
limits.Va = [18,25];            % air relative velocity [m/s]
limits.phi = [-0.43,0.43];      % bank angle [rad]

limits.Aa = [-0.2,0.2];         % acceleration [m/s^2]
limits.phirate = [-1.1,1.1];    % angular velocity [rad/s]

% initialize the time stamp, the state and input reference:
t = [];
xi_ref = [];
u_ref = [];

% define the initial and final conditions:
xi = rand(3,1) .* [(limits.x(2)-limits.x(1)) ; (limits.y(2)-limits.y(1)); (limits.psi(2)-limits.psi(1))] + ... 
                    [limits.x(1) ; limits.y(1); limits.psi(1)];
ui = rand(2,1) .* [(limits.Va(2)-limits.Va(1)) ; (limits.phi(2)-limits.phi(1))] + ... 
                    [limits.Va(1) ; limits.phi(1)];

xf = rand(3,1) .* [(limits.x(2)-limits.x(1)) ; (limits.y(2)-limits.y(1)); (limits.psi(2)-limits.psi(1))] + ... 
                    [limits.x(1) ; limits.y(1); limits.psi(1)];
ui = rand(2,1) .* [(limits.Va(2)-limits.Va(1)) ; (limits.phi(2)-limits.phi(1))] + ... 
                    [limits.Va(1) ; limits.phi(1)];

T = 100;                        % total simulation time [s]


%% Exercises

% Exercise 3.1: Implement the trajectory generation for the UAV model using 
% the flat output representation with a polynomial parameterization

% construct the matrix M * alpha = b

