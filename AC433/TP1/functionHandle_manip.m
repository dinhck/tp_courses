clear all

f = @(x) x.^5 - 3 * x;

I1 = integral(f,1,2);

ezplot(f,[-pi,pi])

g = @(x) 1/x^3 + log(x-2);
